﻿using AutoMapper;
using Backend.Models.Domain;
using Backend.Models.DTO.SquadMember;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Profiles
{
    public class SquadMemberProfile : Profile
    {
        public SquadMemberProfile()
        {
            CreateMap<SquadMember, SquadMemberReadDTO>().ReverseMap();
            CreateMap<SquadMember, SquadMemberCreateDTO>().ReverseMap();
            CreateMap<SquadMember, SquadMemberUpdateDTO>().ReverseMap();
        }
    }
}

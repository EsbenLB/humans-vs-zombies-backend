﻿using AutoMapper;
using Backend.Models.Domain;
using Backend.Models.DTO.SquadCheckin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Profiles
{
    public class SquadCheckinProfile : Profile
    {
        public SquadCheckinProfile()
        {
            //CreateMap<SquadCheckin, SquadCheckinReadDTO>().ForMember(s => s.SquadMemebers, k => k.MapFrom(z = z.SquadMembers.Select(g => g.Id).ToArray())).ReverseMap();
            CreateMap<SquadCheckin, SquadCheckinReadDTO>()
                .ForMember(sdto => sdto.SquadMembers, opt => opt
                    .MapFrom(s => s.SquadMembers
                    .Select(s => s.Id)
                    .ToArray()))
                    .ReverseMap();
            CreateMap<SquadCheckin, SquadCheckinCreateDTO>().ReverseMap();
            CreateMap<SquadCheckin, SquadCheckinUpdateDTO>().ReverseMap();
        }
    }
}

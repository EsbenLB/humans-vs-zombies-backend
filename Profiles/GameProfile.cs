﻿using AutoMapper;
using Backend.Models.Domain;
using Backend.Models.DTO.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Profiles
{
    public class GameProfile : Profile
    {
        public GameProfile()
        {
            CreateMap<Game, GameReadDTO>()
                // Map game.Players
                .ForMember(gdto => gdto.Players, opt => opt
                    .MapFrom(g => g.Players
                    .Select(p => p.Id)
                    .ToArray()))
                // Map game.Missions
                .ForMember(pdto => pdto.Missions, opt => opt
                    .MapFrom(m => m.Missions
                    .Select(m => m.Id)
                    .ToArray()))
                // Map game.Kills
                .ForMember(pdto => pdto.Kills, opt => opt
                    .MapFrom(g => g.Kills
                    .Select(k => k.Id)
                    .ToArray()))
                // Map game.Squads
                .ForMember(pdto => pdto.Squads, opt => opt
                    .MapFrom(g => g.Squads
                    .Select(s => s.Id)
                    .ToArray()))
                // Map game.SquadMembers
                .ForMember(pdto => pdto.SquadMembers, opt => opt
                    .MapFrom(g => g.SquadMembers
                    .Select(s => s.Id)
                    .ToArray()))
                    .ReverseMap();


            CreateMap<Game, GameCreateDTO>().ReverseMap();
            CreateMap<Game, GameUpdateStateDTO>().ReverseMap();
        }
    }
}

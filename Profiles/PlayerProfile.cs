﻿using AutoMapper;
using Backend.Models.Domain;
using Backend.Models.DTO.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Profiles
{
    public class PlayerProfile : Profile
    {
        public PlayerProfile()
        {
            CreateMap<Player, PlayerReadDTO>()
                .ForMember(pdto => pdto.Kills, opt => opt
                    .MapFrom(g => g.Kills
                    .Select(s => s.Id)
                    .ToArray()))
                    .ReverseMap(); 
            CreateMap<Player, PlayerCreateDTO>().ReverseMap();
            CreateMap<Player, PlayerUpdateDTO>().ReverseMap();
        }
    }
}

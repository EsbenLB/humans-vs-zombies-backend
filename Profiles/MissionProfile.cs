﻿using AutoMapper;
using Backend.Models.Domain;
using Backend.Models.DTO.Kill;
using Backend.Models.DTO.Mission;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Profiles
{
    public class MissionProfile : Profile
    {
        public MissionProfile()
        {
            CreateMap<Mission, MissionReadDTO>().ReverseMap();
            CreateMap<Mission, MissionCreateDTO>().ReverseMap();
            CreateMap<Mission, MissionUpdateDTO>().ReverseMap();
        }
    }
}

﻿using AutoMapper;
using Backend.Models.Domain;
using Backend.Models.DTO.Squad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Profiles
{
    public class SquadProfile : Profile
    {
        public SquadProfile()
        {
            CreateMap<Squad, SquadReadDTO>()
                .ForMember(sdto => sdto.SquadMembers, opt => opt
                .MapFrom(s => s.SquadMembers
                .Select(m => m.Id)
                .ToArray()))
                .ReverseMap();
            CreateMap<Squad, SquadCreateDTO>().ReverseMap();
            CreateMap<Squad, SquadUpdateDTO>().ReverseMap();
        }
    }
}

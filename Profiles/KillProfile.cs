﻿using AutoMapper;
using Backend.Models.Domain;
using Backend.Models.DTO.Kill;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Profiles
{
    public class KillProfile : Profile
    {
        public KillProfile()
        {
            CreateMap<Kill, KillReadDTO>().ReverseMap();
            CreateMap<Kill, KillCreateDTO>().ReverseMap();
            CreateMap<Kill, KillUpdateDTO>().ReverseMap();
        }
    }
}

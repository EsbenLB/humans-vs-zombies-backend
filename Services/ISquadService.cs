﻿using Backend.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public interface ISquadService
    {
        public Task<IEnumerable<Squad>> GetAllSquadsAsync();
        public Task<Squad> GetSquadByIdAsync(int id);
        public Task<Squad> AddSquadAsync(Squad squad);
        public Task UpdateSquadAsync(Squad squad);
        public Task DeleteSquadAsync(int id);
        public bool SquadExists(int id);
        public Task<IEnumerable<SquadMember>> GetSquadmemberBySquadId(int Id);
        public Task<IEnumerable<Squad>> GetSquadsByGameId(int Id);

        public Task<IEnumerable<int>> GetKilledSquadMembersId(int squadId);
    }
}

﻿//using Backend.Models;
//using Backend.Models.Domain;
//using Microsoft.EntityFrameworkCore;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

//namespace Backend.Services
//{
//    public class SquadCheckinService : ISquadCheckinService
//    {
//        public readonly HvsZDbContext _context;

//        /// <summary>
//        ///     Constructor
//        /// </summary>
//        /// <param name="context"></param>
//        public SquadCheckinService(HvsZDbContext context)
//        {
//            _context = context;
//        }

//        /// <summary>
//        ///     Method to add a new SquadCheckin to the database
//        /// </summary>
//        /// <param name="squadCheckin"></param>
//        /// <returns></returns>
//        public async Task<SquadCheckin> AddSquadCheckinAsync(SquadCheckin squadCheckin)
//        {
//            _context.SquadCheckins.Add(squadCheckin);
//            await _context.SaveChangesAsync();
//            return squadCheckin;
//        }

//        /// <summary>
//        ///     Method for deleting a SquadCheckin from the databse
//        /// </summary>
//        /// <param name="id"></param>
//        /// <returns></returns>
//        public async Task DeleteSquadCheckin(int id)
//        {
//            var squadCheckin = await _context.SquadCheckins.FindAsync(id);
//            _context.SquadCheckins.Remove(squadCheckin);
//            await _context.SaveChangesAsync();
//        }

//        /// <summary>
//        ///     Gets all the SquadCheckin
//        /// </summary>
//        /// <returns></returns>
//        public async Task<IEnumerable<SquadCheckin>> GetAllSquadCheckinAsync()
//        {
//            return await _context.SquadCheckins.ToListAsync();
//        }

//        /// <summary>
//        ///     Gets a SquadCheckin by primary key
//        /// </summary>
//        /// <param name="id"></param>
//        /// <returns></returns>
//        public async Task<SquadCheckin> GetSquadCheckinAsync(int id)
//        {
//            return await _context.SquadCheckins.FindAsync(id);
//        }

//        /// <summary>
//        ///     Checks to see if a SquadCheckin exists
//        /// </summary>
//        /// <param name="id"></param>
//        /// <returns></returns>
//        public bool SquadCheckinExists(int id)
//        {
//            return _context.SquadCheckins.Any(s => s.Id == id);
//        }

//        /// <summary>
//        ///     Updates the SquadCheckin with new fields
//        /// </summary>
//        /// <param name="squadcheckin"></param>
//        /// <returns></returns>
//        public async Task UpdateSquadCheckinAsync(SquadCheckin squadcheckin)
//        {
//            _context.Entry(squadcheckin).State = EntityState.Modified;
//            await _context.SaveChangesAsync();
//        }
//    }
//}

﻿using Backend.Models;
using Backend.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class KillService : IKillService
    {
        public readonly HvsZDbContext _context;
        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="context"></param>
        public KillService(HvsZDbContext context)
        {
            _context = context;
        }

        /// <summary>
        ///     Method to add a new Kill to the database
        /// </summary>
        /// <param name="kill"></param>
        /// <returns></returns>
        public async Task<Kill> AddKillAsync(Kill kill)
        {
            _context.Kills.Add(kill);
            await _context.SaveChangesAsync();
            return kill;
        }

        /// <summary>
        ///     Method to delete a new Kill from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteKillAsync(int id)
        {
            var kill = await _context.Kills.FindAsync(id);
            _context.Kills.Remove(kill);
            await _context.SaveChangesAsync();
        }

        // hm might need work
        /// <summary>
        ///     Gets all the Kills
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Kill>> GetAllKillsAsync()
        {
            return await _context.Kills.ToListAsync();
        }

        /// <summary>
        ///     Gets a Kill by primary key
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Kill> GetKillAsync(int id)
        {
            return await _context.Kills.FindAsync(id);
        }

        /// <summary>
        ///     Checks to see if a Kill exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool KillExists(int id)
        {
            return _context.Kills.Any(c => c.Id == id);
        }

        /// <summary>
        ///     Updates the Kill with new fields
        /// </summary>
        /// <param name="kill"></param>
        /// <returns></returns>
        public async Task UpdateKillAsync(Kill kill)
        {
            _context.Entry(kill).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }


    }
}

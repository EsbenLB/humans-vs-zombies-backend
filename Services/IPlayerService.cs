﻿using Backend.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public interface IPlayerService
    {
        public Task<IEnumerable<Player>> GetAllPlayersAsync();
        public Task<Player> GetPlayerAsync(int id);
        public Task<Player> AddPlayerAsync(Player player);
        public Task UpdatePlayerAsync(Player player);
        public Task UpdatePlayerGameAsync(int playerId, int gameId);
        public Task UpdatePlayerUserAsync(int playerId, int userId);
        public Task DeletePlayerAsync(int id);
        public bool PlayerExists(int id);
        public Task<Player> GetPlayerByBiteCode(string biteCode);
        public Task<IEnumerable<Player>> GetPlayerByGameId(int Id);
    }
}

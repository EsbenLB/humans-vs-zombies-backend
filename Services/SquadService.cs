﻿using Backend.Models;
using Backend.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class SquadService : ISquadService
    {
        public readonly HvsZDbContext _context;

        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="context"></param>
        public SquadService(HvsZDbContext context)
        {
            _context = context;
        }

        /// <summary>
        ///     Method to add a new Squad to the database
        /// </summary>
        /// <param name="squad"></param>
        /// <returns></returns>
        public async Task<Squad> AddSquadAsync(Squad squad)
        {
            _context.Squads.Add(squad);
            await _context.SaveChangesAsync();
            return squad;
        }

        /// <summary>
        ///     Method for deleting a Squad from the databse
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteSquadAsync(int id)
        {
            var squad = await _context.Squads.FindAsync(id);
            _context.Squads.Remove(squad);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        ///     Gets all the Squad
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Squad>> GetAllSquadsAsync()
        {
            return await _context.Squads.Include(s => s.SquadMembers).ToListAsync();
            //return await _context.Squads.ToListAsync();
        }

        /// <summary>
        ///     Gets a Squad by primary key
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Squad> GetSquadByIdAsync(int id)
        {
            return await _context.Squads.Include(s => s.SquadMembers).FirstOrDefaultAsync(s => s.Id == id);
        }

        /// <summary>
        ///     Checks to see if a Squad exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool SquadExists(int id)
        {
            return _context.Squads.Any(c => c.Id == id);
        }

        /// <summary>
        ///     Updates the Squad with new fields
        /// </summary>
        /// <param name="squad"></param>
        /// <returns></returns>
        public async Task UpdateSquadAsync(Squad squad)
        {
            _context.Entry(squad).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        ///     Get all squadmember in squad with squadmember id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<IEnumerable<SquadMember>> GetSquadmemberBySquadId(int Id)
        {
            return await _context.SquadMembers.Where(s => s.SquadId == Id).ToListAsync<SquadMember>();
        }

        /// <summary>
        ///     Get all squads in game with game id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Squad>> GetSquadsByGameId(int Id)
        {
            return await _context.Squads.Where(p => p.GameId == Id).Include(s => s.SquadMembers).ToListAsync<Squad>();
        }

        /// <summary>
        ///  Get all killed members in squad
        /// </summary>
        /// <param name="squadId"></param
        /// <returns></returns>
        public async Task<IEnumerable<int>> GetKilledSquadMembersId(int squadId)
        {
            var killedMembers = await (from sm in _context.SquadMembers 
                                       join k in _context.Kills on sm.PlayerId equals k.VictimId
                                       where sm.SquadId == squadId
                                       select sm.Id)
                                       .ToListAsync<int>();
            return killedMembers;
        }

    }
}

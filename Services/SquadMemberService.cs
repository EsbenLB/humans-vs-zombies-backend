﻿using Backend.Models;
using Backend.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class SquadMemberService : ISquadMemberService
    {
        private readonly HvsZDbContext _context;
        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="context"></param>
        public SquadMemberService(HvsZDbContext context)
        {
            _context = context;
        }

        /// <summary>
        ///     Method to add a new SquadMember to the database
        /// </summary>
        /// <param name="squadMember"></param>
        /// <returns></returns>
        public async Task<SquadMember> AddSquadMemberAsync(SquadMember squadMember)
        {
            _context.SquadMembers.Add(squadMember);
            await _context.SaveChangesAsync();
            return squadMember;
        }

        /// <summary>
        ///     Method for deleting a SquadMember from the databse
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteSquadMember(int id)
        {
            var squadMember = await _context.SquadMembers.FindAsync(id);
            _context.SquadMembers.Remove(squadMember);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        ///      Gets all the SquadMembers
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<SquadMember>> GetAllSquadMembersAsync()
        {
            return await _context.SquadMembers.ToListAsync();
        }

        /// <summary>
        ///     Gets a SquadMembers by primary key
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<SquadMember> GetSquadMemberAsync(int id)
        {
            return await _context.SquadMembers.FindAsync(id);
        }

        /// <summary>
        ///     Gets all SquadMembers by playerId
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<SquadMember>> GetSquadMembersByPlayerIdAsync(int playerId)
        {
            return await _context.SquadMembers.Where(s => s.PlayerId == playerId).ToListAsync<SquadMember>();
        }

        /// <summary>
        ///     Checks to see if a SquadMembers exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool SquadMemberExists(int id)
        {
            return _context.SquadMembers.Any(s => s.Id == id);
        }

        /// <summary>
        ///     Updates the SquadMembers with new fields
        /// </summary>
        /// <param name="squadMember"></param>
        /// <returns></returns>
        public async Task UpdateSquadMember(SquadMember squadMember)
        {
            _context.Entry(squadMember).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}

﻿using Backend.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    /// <summary>
    /// Service interface for database manipulation of Franchise
    /// </summary>
    public interface IGameService
    {
        public Task<IEnumerable<Game>> GetAllGamesAsync();
        public Task<Game> GetGameByIdAsync(int id);
        public Task<Game> AddGameAsync(Game game);
        public Task UpdateGameAsync(Game game);
        public Task DeleteGameAsync(int id);
        public bool GameExists(int id);
    }
}

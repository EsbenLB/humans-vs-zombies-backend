﻿using Backend.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    /// <summary>
    /// Service interface for database manipulation of Franchise
    /// </summary>
    public interface IMissionService
    {
        public Task<IEnumerable<Mission>> GetAllMissionsAsync();
        public Task<Mission> GetMissionAsync(int id);
        public Task<Mission> AddMissionAsync(Mission mission);
        public Task UpdateMissionAsync(Mission mission);
        public Task DeleteMissionAsync(int id);
        public bool MissionExists(int id);
        public Task<IEnumerable<Mission>> GetMissionsByGameId(int Id);
    }
}

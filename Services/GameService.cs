﻿using Backend.Models;
using Backend.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    /// <summary>
    /// Service/repository for database manipulation
    /// </summary>
    public class GameService : IGameService
    {
        public readonly HvsZDbContext _context;
        
        /// <summary>
        ///      Constructor
        /// </summary>
        /// <param name="context"></param>
        public GameService(HvsZDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Gets all the Games
        /// </summary>
        /// <returns>List of Games</returns>
        public async Task<IEnumerable<Game>> GetAllGamesAsync()
        {
            return await _context.Games.Include(g => g.Players).Include(g => g.Missions).Include(g => g.Squads)
                .Include(g => g.SquadMembers).Include(g => g.Kills).ToListAsync();
        }
        /// <summary>
        /// Gets a Game by primary key
        /// </summary>
        /// <param name="id">the primary key for the Game</param>
        /// <returns>the Task to get the Game</returns>
        public async Task<Game> GetGameByIdAsync(int id)
        {
            return await _context.Games.Include(g => g.Players).Where(p => p.Id == id).FirstOrDefaultAsync();
        }
        /// <summary>
        /// Method to add a new Game to the database
        /// </summary>
        /// <param name="game">A Game object</param>
        /// <returns>Asynchronous task to add the Game</returns>
        public async Task<Game> AddGameAsync(Game game)
        {
            _context.Games.Add(game);
            await _context.SaveChangesAsync();
            return game;
        }
        /// <summary>
        /// Method for deleting a Game from the databse
        /// </summary>
        /// <param name="id">primary key for the Game</param>
        /// <returns>Asynchronous task for deleting the Game</returns>
        public async Task DeleteGameAsync(int id)
        {
            var game = await _context.Games.FindAsync(id);
            _context.Games.Remove(game);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Updates the Game with new fields
        /// </summary>
        /// <param name="game">Game object</param>
        /// <returns>async operation</returns>
        public async Task UpdateGameAsync(Game game)
        {
            _context.Entry(game).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Checks to see if a Game exists
        /// </summary>
        /// <param name="id">primary key for the game you want to find</param>
        /// <returns>true or false depending on success</returns>
        public bool GameExists(int id)
        {
            return _context.Games.Any(g => g.Id == id);
        }
    }
}

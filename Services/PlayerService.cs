﻿using Backend.Models;
using Backend.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class PlayerService : IPlayerService
    {
        private readonly HvsZDbContext _context;

        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="context"></param>
        public PlayerService(HvsZDbContext context)
        {
            _context = context;
        }

        /// <summary>
        ///     Method to add a new Player to the database
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public async Task<Player> AddPlayerAsync(Player player)
        {
            _context.Players.Add(player);
            await _context.SaveChangesAsync();
            return player;
        }

        /// <summary>
        ///     Method for deleting a Player from the databse
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeletePlayerAsync(int id)
        {
            var player = await _context.Players.FindAsync(id);
            
            _context.Players.Remove(player);
            await _context.SaveChangesAsync();
        }

        // watch me! tolistasync??
        /// <summary>
        ///     Gets all the Players
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Player>> GetAllPlayersAsync()
        {
            return await _context.Players
                .Include(p => p.User)
                .Include(p => p.Game)
                .Include(p => p.Kills)
                .ToListAsync();
        }

        /// <summary>
        ///     Gets a Player by primary key
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Player> GetPlayerAsync(int id)
        {
            return await _context.Players.FindAsync(id);
        }

        /// <summary>
        ///    Checks to see if a Player exists 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool PlayerExists(int id)
        {
            return _context.Players.Any(p => p.Id == id);
        }

        /// <summary>
        ///     Updates the Player with new fields
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public async Task UpdatePlayerAsync(Player player)
        {
            _context.Entry(player).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        ///     Updates the Players in a Game
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="gameId"></param>
        /// <returns></returns>
        public async Task UpdatePlayerGameAsync(int playerId, int gameId)
        {
            Player playerToUpdate = await _context.Players
                .Include(p => p.Game)
                .Where(p => p.Id == playerId)
                .FirstAsync();

            Game game = await _context.Games.FindAsync(gameId);

            if (game == null)
                throw new KeyNotFoundException();

            playerToUpdate.Game = game;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        ///  Updates the Players to a User
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task UpdatePlayerUserAsync(int playerId, int userId)
        {
            Player playerToUpdate = await _context.Players
                .Include(p => p.User)
                .Where(p => p.Id == playerId)
                .FirstAsync();

            User user = await _context.Users.FindAsync(userId);

            if (user == null)
                throw new KeyNotFoundException("There exist no user with the given ID: " + userId);

            playerToUpdate.User = user;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        ///     Gets a Player by bitecode
        /// </summary>
        /// <param name="biteCode"></param>
        /// <returns></returns>
        public async Task<Player> GetPlayerByBiteCode(string biteCode)
        {
            //string sql = $"SELECT * FROM Players WHERE BiteCode = '{biteCode};'";

            var players = await _context.Players.Where(p => p.BiteCode == biteCode).ToListAsync<Player>();

            if (players.Count() == 0)
            {
                throw new KeyNotFoundException("There exists no players with the given BiteCode: " + biteCode);
            }

            var player = players.First();

            return player;
            //return await _context.Players.FromSqlRaw(sql).ToListAsync();
        }

        /// <summary>
        ///     Get all players in game with game id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Player>> GetPlayerByGameId(int Id)
        {
           
            return await _context.Players.Where(p => p.GameId == Id).ToListAsync<Player>();
        }
    }
}

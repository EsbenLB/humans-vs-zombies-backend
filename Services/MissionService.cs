﻿using Backend.Models;
using Backend.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class MissionService : IMissionService
    {
        private readonly HvsZDbContext _context;

        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="context"></param>
        public MissionService(HvsZDbContext context)
        {
            _context = context;
        }

        /// <summary>
        ///     Method to add a new Mission to the database
        /// </summary>
        /// <param name="mission"></param>
        /// <returns></returns>
        public async Task<Mission> AddMissionAsync(Mission mission)
        {
            _context.Missions.Add(mission);
            await _context.SaveChangesAsync();
            return mission;
        }

        /// <summary>
        ///     Method for deleting a Mission from the databse
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteMissionAsync(int id)
        {
            var misssion = await _context.Missions.FindAsync(id);
            _context.Missions.Remove(misssion);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        ///     Gets all the Missions
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Mission>> GetAllMissionsAsync()
        {
            return await _context.Missions.ToListAsync();
        }

        /// <summary>
        ///     Gets a Mission by primary key
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Mission> GetMissionAsync(int id)
        {
            return await _context.Missions.FindAsync(id);
        }

        /// <summary>
        ///     Updates the Mission with new fields
        /// </summary>
        /// <param name="misssion"></param>
        /// <returns></returns>
        public async Task UpdateMissionAsync(Mission misssion)
        {
            _context.Entry(misssion).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        ///     Checks to see if a Mission exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool MissionExists(int id)
        {
            return _context.Missions.Any(u => u.Id == id);
        }

        /// <summary>
        ///     Gets all the Missions in a Game, by the Game primary key (id)
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Mission>> GetMissionsByGameId(int Id)
        {

            return await _context.Missions.Where(p => p.GameId == Id).ToListAsync<Mission>();
        }
    }
}

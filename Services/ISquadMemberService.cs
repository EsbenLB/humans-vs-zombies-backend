﻿using Backend.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public interface ISquadMemberService
    {
        public Task<IEnumerable<SquadMember>> GetAllSquadMembersAsync();
        public Task<SquadMember> GetSquadMemberAsync(int id);
        public Task<SquadMember> AddSquadMemberAsync(SquadMember squadMember);
        public Task UpdateSquadMember(SquadMember squadMember);
        public Task DeleteSquadMember(int id);
        public bool SquadMemberExists(int id);
        public Task<IEnumerable<SquadMember>> GetSquadMembersByPlayerIdAsync(int playerId);
    }
}

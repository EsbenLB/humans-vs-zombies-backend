﻿using Backend.Models;
using Backend.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class UserService : IUserService
    {
        private readonly HvsZDbContext _context;

        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="context"></param>
        public UserService(HvsZDbContext context)
        {
            _context = context;
        }

        /// <summary>
        ///     Method to add a new User to the database
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<User> AddUserAsync(User user)
        {
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
            return user;
        }

        /// <summary>
        ///     Method for deleting a User from the databse
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteUserAsync(string id)
        {
            var user = await _context.Users.FindAsync(id);
            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        ///     Gets all the User
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<User>> GetAllUsersAsync()
        {
            
            return await _context.Users.Include(u => u.Players).ToListAsync();
        }

        /// <summary>
        ///     Gets a User by primary key
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public async Task<User> GetUserAsync(string username)
        {
            return await _context.Users.Include(u => u.Players).Where(u => u.Id == username).FirstOrDefaultAsync();
        }

        /// <summary>
        ///     Updates the User with new fields
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task UpdateUserAsync(User user)
        {
            _context.Entry(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        ///     Checks to see if a User exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool UserExists(string id)
        {
            return _context.Users.Any(u => u.Id == id);
        }
    }
}

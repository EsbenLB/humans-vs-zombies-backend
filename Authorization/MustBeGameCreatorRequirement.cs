﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Authorization
{
    public class MustBeGameCreatorRequirement : IAuthorizationRequirement
    {
        public MustBeGameCreatorRequirement()
        {

        }
    }
}

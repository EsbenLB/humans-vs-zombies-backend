﻿using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Backend.Authorization
{
    public class MustBeGameCreatorHandler : AuthorizationHandler<MustBeGameCreatorRequirement>
    {
        private readonly IGameService _gameService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public MustBeGameCreatorHandler(IGameService gameService, IHttpContextAccessor httpContextAccessor)
        {
            _gameService = gameService;
            _httpContextAccessor = httpContextAccessor;
        }

        protected async override Task HandleRequirementAsync(AuthorizationHandlerContext context,
            MustBeGameCreatorRequirement requirement)
        {
            if (!context.User.Identity.IsAuthenticated)
            {
                context.Fail();
                return;
            }

            var gameId = _httpContextAccessor.HttpContext.Request.RouteValues["gameId"];
            int gameIdAsInt = Convert.ToInt32(gameId);

            var userId = context.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            // might not work, look out
            var userIdAsInt = Convert.ToInt32(userId);


            var game = await _gameService.GetGameByIdAsync(gameIdAsInt);
            if (game == null)
            {
                // Let it through so the controller can return 404
                context.Succeed(requirement);
                return;
            }

            // need to add game creator as a field
            // if (game.UserId != userIdAsInt)
            // {
            //     context.Fail();
            //     return;
            // }

            context.Succeed(requirement);
        } 
    }
}

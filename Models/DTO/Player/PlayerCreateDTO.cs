﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models.DTO.Player
{
    public class PlayerCreateDTO
    {
        public bool IsHuman { get; set; }
        public bool IsPatientZero { get; set; }

        public string UserId { get; set; }
        public int GameId { get; set; }
    }
}

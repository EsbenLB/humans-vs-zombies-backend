﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models.DTO.Player
{
    public class PlayerUpdateDTO
    {
        public bool IsHuman { get; set; }
        public bool IsPatientZero { get; set; }
    }
}

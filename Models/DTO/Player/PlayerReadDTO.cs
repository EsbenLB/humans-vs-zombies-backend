﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models.DTO.Player
{
    public class PlayerReadDTO
    {
        public int Id { get; set; }
        public bool IsHuman { get; set; }
        public bool IsPatientZero { get; set; }
        public string BiteCode { get; set; }

        public string UserId { get; set; }
        public int GameId { get; set; }
        public List<int> Kills { get; set; }
    }
}

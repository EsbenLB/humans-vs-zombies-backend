﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models.DTO.Squad
{
    public class SquadUpdateDTO
    {
        public string Name { get; set; }
        public bool IsHuman { get; set; }
        public int GameId { get; set; }
        //public ICollection<SquadMember> SquadMembers { get; set; }
    }
}

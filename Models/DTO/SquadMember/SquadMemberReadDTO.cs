﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models.DTO.SquadMember
{
    public class SquadMemberReadDTO
    {
        public int Id { get; set; }
        public int Rank { get; set; }
        public int GameId { get; set; }
        public int SquadId { get; set; }
        public int PlayerId { get; set; }

    }
}

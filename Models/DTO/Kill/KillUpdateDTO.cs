﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models.DTO.Kill
{
    public class KillUpdateDTO
    {
        public DateTime TimeOfDeath { get; set; }
        public string Story { get; set; }
        public double Lat { get; set; }
        public double Ing { get; set; }
        // FK
        public int GameId { get; set; }
        // FK
        public int KillerId { get; set; }
        // FK
        public int VictimId { get; set; }
    }
}

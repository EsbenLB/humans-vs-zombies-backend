﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models.DTO.User
{
    public class UserReadDTO
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<int> Players { get; set;}
        //public string Password { get; set; }
        public bool IsAdmin { get; set; }
    }
}

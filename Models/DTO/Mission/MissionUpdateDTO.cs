﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models.DTO.Mission
{
    public class MissionUpdateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsHumanVisible { get; set; }

        public bool IsZombieVisible { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime DateTime { get; set; }
        public float Lat { get; set; }
        public float Ing { get; set; }

        public int GameId { get; set; }
    }
}

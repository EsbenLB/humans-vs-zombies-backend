﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models.DTO.SquadCheckin
{
    public class SquadCheckinUpdateDTO
    {
        public DateTime StartTime { get; set; }
        public DateTime DateTime { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public int GameId { get; set; }
        //public Game Game { get; set; }
        public int SquadId { get; set; }
        //public Squad Squad { get; set; }
        public int SquadMemberId { get; set; }
        public List<int> SquadMembers { get; set; }
    }
}

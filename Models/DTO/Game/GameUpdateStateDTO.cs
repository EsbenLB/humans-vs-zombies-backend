﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models.DTO.Game
{
    public class GameUpdateStateDTO
    {
        public string GameState { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models.DTO.Game
{
    public class GameReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string GameState { get; set; }
        public string Description { get; set; }
        public double Nw_Lat { get; set; }
        public double Nw_Ing { get; set; }
        public double Se_Lat { get; set; }
        public double Se_Ing { get; set; }
        public List<int> Players { get; set; }
        public List<int> Missions { get; set; }
        public List<int> Kills { get; set; }
        public List<int> Squads { get; set; }
        public List<int> SquadMembers { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models.DTO.Game
{
    public class GameCreateDTO
    {
        public string Name { get; set; }
        public string GameState { get; set; }
        public string Description { get; set; }
        public double Nw_Lat { get; set; }
        public double Nw_Ing { get; set; }
        public double Se_Lat { get; set; }
        public double Se_Ing { get; set; }
    }
}

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Models.Domain
{
    
    public class Squad
    {
        // PK
        [Key]
        public int Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        public bool IsHuman { get; set; }
        
        [Required]
        [ForeignKey("GameId")]
        public int GameId { get; set; }
        public Game Game { get; set; }
        // --- choice 
        // FK
        //public int ChatId { get; set; }
        //public Chat Chat { get; set; }
        // FK
        public ICollection<SquadMember> SquadMembers { get; set; }

        
    }
}


using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Models.Domain
{

    public class Mission
    {
        // PK
        [Key]
        public int Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(50)]
        public string Description { get; set; }
        public bool IsHumanVisible { get; set; }
        public bool IsZombieVisible { get; set; }

        public DateTime StartTime { get; set; }
        public DateTime DateTime { get; set; }
        // Position
        public float Lat { get; set; }
        public float Ing { get; set; }
        // FK
        [Required]
        [ForeignKey("GameId")]
        public int GameId { get; set; }
        public Game Game { get; set; }


    }
}


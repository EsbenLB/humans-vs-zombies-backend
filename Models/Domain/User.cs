using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Backend.Models.Domain
{
    
    public class User
    {
        // PK
        [Key]
        [MaxLength(50)]
        public string Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }
        [Required]
        public bool IsAdmin { get; set; }
        // FK
        public ICollection <Player> Players { get; set; }
        

    }
}


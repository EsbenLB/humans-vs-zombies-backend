using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Models.Domain
{
    
    public class Kill
    {
        // PK
        [Key]
        public int Id { get; set; }
        public DateTime TimeOfDeath { get; set; }
        public string Story { get; set; }
        public double Lat { get; set; }
        public double Ing { get; set; }

        // FK
        [Required]
        [ForeignKey("GameId")]
        public int GameId { get; set; }
        public Game Game { get; set; }

        // FK
        [Required]
        [ForeignKey("KillerId")]
        public int KillerId { get; set; }
        public Player Killer { get; set; }

        // FK
        [Required]
        [ForeignKey("VictimId")]
        public int VictimId { get; set; }
        public Player Victim { get; set; }
        

    }
}


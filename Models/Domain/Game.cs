using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Backend.Models.Domain
{
    
    public class Game
    {
        // PK
        [Key]
        public int Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(50)]
        public string GameState { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }

        // Coordinates
        public double Nw_Lat { get; set; }
        public double Nw_Ing { get; set; }
        public double Se_Lat { get; set; }
        public double Se_Ing { get; set; }
        // FK
        public ICollection <Player> Players { get; set;}
        // FK
        public ICollection<Kill> Kills { get; set; }
        // FK
        public ICollection<Mission> Missions { get; set; }
        // FK
        public ICollection<Squad> Squads { get; set; }
       // FK
        public ICollection<SquadMember> SquadMembers { get; set; }
    }
}


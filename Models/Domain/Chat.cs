using System;
using System.ComponentModel.DataAnnotations;

namespace Backend.Models.Domain
{
    
    public class Chat
    {
        // PK
        [Key]
        public int Id { get; set; }
        [MaxLength(50)]
        public string Message { get; set; }
        public bool IsHumanGlobal { get; set; }
        public bool IsZombieGlobal { get; set; }
        public DateTime ChatTime { get; set; }
        // FK
        public int GameId { get; set; }
        // FK
        public int PlayerId { get; set; }
        // FK
        public int SquadId { get; set; }


    }
}


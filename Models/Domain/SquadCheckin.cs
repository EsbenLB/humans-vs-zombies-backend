using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Models.Domain
{
    
    public class SquadCheckin
    {
        // PK
        [Key]
        public int Id { get; set; }
        
        public DateTime StartTime { get; set; }
        public DateTime DateTime { get; set; }

        public double Lat { get; set; }
        public double Lng { get; set; }
        [Required]
        [ForeignKey("GameId")]
        public int GameId { get; set; }
        public Game Game { get; set; }
        [Required]
        [ForeignKey("SquadId")]
        public int SquadId { get; set; }
        public Squad Squad { get; set; }
        [Required]
        [ForeignKey("SquadMemberId")]
        public int SquadMemberId { get; set; }
        public ICollection<SquadMember> SquadMembers { get; set; }



    }
}


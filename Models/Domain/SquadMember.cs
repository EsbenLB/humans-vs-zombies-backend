using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Models.Domain
{

    public class SquadMember
    {
        // PK
        [Key]
        public int Id { get; set; }
        public int Rank { get; set; }
        [Required]
        [ForeignKey("GameId")]
        public int GameId { get; set; }
        public Game Game { get; set; }
        [Required]
        [ForeignKey("SquadId")]
        public int SquadId { get; set; }
        public Squad Squad { get; set; }
        [Required]
        [ForeignKey("PlayerId")]
        public int PlayerId { get; set; } 
        public Player Player { get; set; }
    }
}
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Models.Domain
{
   
    public class Player
    {
        // PK
        [Key]
        public int Id { get; set; }
        public bool IsHuman { get; set; }
        public bool IsPatientZero { get; set; }
        public string BiteCode { get; set; }

        [Required]
        [ForeignKey("UserId")]
        public string UserId { get; set; }
        public User User { get; set; }
        [Required]
        [ForeignKey("GameId")]
        public int GameId { get; set; }
        public Game Game { get; set; }
        // --- choice 
        // FK
        //public int ChatId { get; set; }
        //public Chat Chat { get; set; }

        // FK, Killed by another player
        public Kill Death { get; set; }

        // FK, All Player killed by this player
        public ICollection<Kill> Kills { get; set; }
        public SquadMember SquadMember { get; set; }

    }
}


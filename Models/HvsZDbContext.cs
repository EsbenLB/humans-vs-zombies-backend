﻿
using Backend.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace Backend.Models
{
    /// <summary>
    /// Where the database with tables and seeded data is created/configured
    /// </summary>
    public class HvsZDbContext : DbContext
    {
        public DbSet<Game> Games { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Kill> Kills { get; set; }
        public DbSet<Mission> Missions { get; set; }
        public DbSet<Squad> Squads { get; set; }
        //public DbSet<SquadCheckin> SquadCheckins { get; set; }
        public DbSet<SquadMember> SquadMembers { get; set; }
        public HvsZDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Player>()
                .HasOne<SquadMember>(s => s.SquadMember)
                .WithOne(p => p.Player)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<Player>()
                .HasIndex(p => p.BiteCode)
                .IsUnique();
            modelBuilder.Entity<Mission>()
                .HasOne<Game>(k => k.Game)
                .WithMany(m => m.Missions)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<Squad>()
                .HasOne<Game>(s => s.Game)
                .WithMany(p => p.Squads)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<SquadMember>()
                .HasOne<Squad>(s => s.Squad)
                .WithMany(p => p.SquadMembers)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<SquadMember>()
                .HasOne<Game>(s => s.Game)
                .WithMany(p => p.SquadMembers)
                .OnDelete(DeleteBehavior.NoAction);

            //modelBuilder.Entity<SquadCheckin>()
            //    .HasMany<SquadMember>(s => s.SquadMembers)
            //    .WithOne(p => p.SquadCheckin)
            //    .OnDelete(DeleteBehavior.NoAction);

            //modelBuilder.Entity<SquadCheckin>()
            //    .HasOne<Squad>(s => s.Squad)
            //    .WithMany(p => p.SquadCheckin)
            //    .OnDelete(DeleteBehavior.NoAction);

            
            
            //modelBuilder.Entity<Kill>()
            //    .HasOne<Player>(k => k.Killer)
            //    .WithMany(p => p.Kills)
            //    .HasForeignKey(k => k.KillerId);

            // On deleting Kill
            modelBuilder.Entity<Kill>()
                .HasOne(k => k.Killer)
                .WithMany(p => p.Kills)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<Kill>()
                .HasOne(k => k.Victim)
                .WithOne(p => p.Death)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<Kill>()
                .HasOne<Game>(g => g.Game)
                .WithMany(k => k.Kills)
                .OnDelete(DeleteBehavior.NoAction);



            // Seed data
            // Game
            modelBuilder.Entity<Game>().HasData(new Game
            {
            
                Id = 1,
                Name = "Zombie game 001",
                Description = "This is a default dummy game set in fantasyland (or probably in a real location once we have implemented the map).",
                GameState = "Registration",
                Se_Lat = 60.391000,
                Se_Ing = 5.327330,
                Nw_Lat = 5,
                Nw_Ing = 5,


            }); 
            modelBuilder.Entity<Game>().HasData(new Game
            {
                Id = 2,
                Name = "Zombie game 002",
                Description = "This is a second default dummy game. This one takes place in fantasyland 2 (or probably in a real location once we have implemented the map).",
                GameState = "Registration",
                Se_Lat = 65.391000,
                Se_Ing = 10.327330,
                Nw_Lat = 10,
                Nw_Ing = 10,

            });
            // Users
            modelBuilder.Entity<User>().HasData(new User
            {
                Id = "bobleif@example.com",
                FirstName = "bob",
                LastName = "leif",
                IsAdmin = false

            });
            modelBuilder.Entity<User>().HasData(new User
            {
                Id = "peternissan@example.com",
                FirstName = "Peter",
                LastName = "Nissan",
                IsAdmin = false

            });
            modelBuilder.Entity<User>().HasData(new User
            {
                Id = "karifjord@example.com",
                FirstName = "Kari",
                LastName = "Fjord",
                IsAdmin = true

            });
            // Players
            modelBuilder.Entity<Player>().HasData(new Player
            {
                Id = 1,
                IsHuman = true,
                IsPatientZero = false,
                BiteCode = "AAA",
                GameId = 1,
                UserId = "bobleif@example.com"
            }); 
            modelBuilder.Entity<Player>().HasData(new Player
            {
                Id = 2,
                IsHuman = true,
                IsPatientZero = false,
                BiteCode = "BBB",
                GameId = 1,
                UserId = "peternissan@example.com"
            });
            modelBuilder.Entity<Player>().HasData(new Player
            {
                Id = 3,
                IsHuman = true,
                IsPatientZero = false,
                BiteCode = "CCC",
                GameId = 2,
                UserId = "bobleif@example.com"
            });
            modelBuilder.Entity<Player>().HasData(new Player
            {
                Id = 4,
                IsHuman = false,
                IsPatientZero = true,
                BiteCode = "DDD",
                GameId = 2,
                UserId = "peternissan@example.com"
            });
            // Mission
            modelBuilder.Entity<Mission>().HasData(new Mission
            {
                Id = 1,
                Name = "easy mission 1",
                Description = "Mission desc",
                IsHumanVisible = true,
                IsZombieVisible = false,
                StartTime = new System.DateTime(2014, 12, 25),
                DateTime = new System.DateTime(2015, 12, 25),
                Lat = 60.391217f,
                Ing = 5.327336f,
                GameId = 1

            });
            modelBuilder.Entity<Mission>().HasData(new Mission
            {
                Id = 2,
                Name = "easy mission 2",
                Description = "Mission desc 2",
                IsHumanVisible = true,
                IsZombieVisible = false,
                StartTime = new System.DateTime(2014, 11, 25),
                DateTime = new System.DateTime(2015, 11, 25),
                Lat = 60.391217f,
                Ing = 5.327336f,
                GameId = 1

            });
            // Squad
            modelBuilder.Entity<Squad>().HasData(new Squad
            {
                Id = 1,
                Name = "The Squids",
                IsHuman = false,
                GameId = 1,
            });
            modelBuilder.Entity<Squad>().HasData(new Squad
            {
                Id = 2,
                Name = "The Terrifiers",
                IsHuman = true,
                GameId = 2,
            });
            // SquadMember
            modelBuilder.Entity<SquadMember>().HasData(new SquadMember
            {
                Id = 1,
                Rank = 9,
                GameId = 1,
                SquadId = 1,
                PlayerId = 1


            });
            modelBuilder.Entity<SquadMember>().HasData(new SquadMember
            {
                Id = 2,
                Rank = 8,
                GameId = 2,
                SquadId = 2,
                PlayerId = 2
            });
            
        }
    }
}

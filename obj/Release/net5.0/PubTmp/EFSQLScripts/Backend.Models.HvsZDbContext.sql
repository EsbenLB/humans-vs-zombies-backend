﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    CREATE TABLE [Games] (
        [Id] int NOT NULL IDENTITY,
        [Name] nvarchar(50) NULL,
        [GameState] nvarchar(50) NULL,
        [Description] nvarchar(500) NULL,
        [Nw_Lat] float NOT NULL,
        [Nw_Ing] float NOT NULL,
        [Se_Lat] float NOT NULL,
        [Se_Ing] float NOT NULL,
        CONSTRAINT [PK_Games] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    CREATE TABLE [Users] (
        [Id] nvarchar(50) NOT NULL,
        [FirstName] nvarchar(50) NOT NULL,
        [LastName] nvarchar(50) NOT NULL,
        [IsAdmin] bit NOT NULL,
        CONSTRAINT [PK_Users] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    CREATE TABLE [Missions] (
        [Id] int NOT NULL IDENTITY,
        [Name] nvarchar(50) NULL,
        [Description] nvarchar(50) NULL,
        [IsHumanVisible] bit NOT NULL,
        [IsZombieVisible] bit NOT NULL,
        [StartTime] datetime2 NOT NULL,
        [DateTime] datetime2 NOT NULL,
        [Lat] real NOT NULL,
        [Ing] real NOT NULL,
        [GameId] int NOT NULL,
        CONSTRAINT [PK_Missions] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Missions_Games_GameId] FOREIGN KEY ([GameId]) REFERENCES [Games] ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    CREATE TABLE [Squads] (
        [Id] int NOT NULL IDENTITY,
        [Name] nvarchar(50) NULL,
        [IsHuman] bit NOT NULL,
        [GameId] int NOT NULL,
        CONSTRAINT [PK_Squads] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Squads_Games_GameId] FOREIGN KEY ([GameId]) REFERENCES [Games] ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    CREATE TABLE [Players] (
        [Id] int NOT NULL IDENTITY,
        [IsHuman] bit NOT NULL,
        [IsPatientZero] bit NOT NULL,
        [BiteCode] nvarchar(450) NULL,
        [UserId] nvarchar(50) NOT NULL,
        [GameId] int NOT NULL,
        CONSTRAINT [PK_Players] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Players_Games_GameId] FOREIGN KEY ([GameId]) REFERENCES [Games] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_Players_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    CREATE TABLE [Kills] (
        [Id] int NOT NULL IDENTITY,
        [TimeOfDeath] datetime2 NOT NULL,
        [Story] nvarchar(max) NULL,
        [Lat] float NOT NULL,
        [Ing] float NOT NULL,
        [GameId] int NOT NULL,
        [KillerId] int NOT NULL,
        [VictimId] int NOT NULL,
        CONSTRAINT [PK_Kills] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Kills_Games_GameId] FOREIGN KEY ([GameId]) REFERENCES [Games] ([Id]),
        CONSTRAINT [FK_Kills_Players_KillerId] FOREIGN KEY ([KillerId]) REFERENCES [Players] ([Id]),
        CONSTRAINT [FK_Kills_Players_VictimId] FOREIGN KEY ([VictimId]) REFERENCES [Players] ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    CREATE TABLE [SquadMembers] (
        [Id] int NOT NULL IDENTITY,
        [Rank] int NOT NULL,
        [GameId] int NOT NULL,
        [SquadId] int NOT NULL,
        [PlayerId] int NOT NULL,
        CONSTRAINT [PK_SquadMembers] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_SquadMembers_Games_GameId] FOREIGN KEY ([GameId]) REFERENCES [Games] ([Id]),
        CONSTRAINT [FK_SquadMembers_Players_PlayerId] FOREIGN KEY ([PlayerId]) REFERENCES [Players] ([Id]),
        CONSTRAINT [FK_SquadMembers_Squads_SquadId] FOREIGN KEY ([SquadId]) REFERENCES [Squads] ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Description', N'GameState', N'Name', N'Nw_Ing', N'Nw_Lat', N'Se_Ing', N'Se_Lat') AND [object_id] = OBJECT_ID(N'[Games]'))
        SET IDENTITY_INSERT [Games] ON;
    EXEC(N'INSERT INTO [Games] ([Id], [Description], [GameState], [Name], [Nw_Ing], [Nw_Lat], [Se_Ing], [Se_Lat])
    VALUES (1, N''This is a default dummy game set in fantasyland (or probably in a real location once we have implemented the map).'', N''Registration'', N''Zombie game 001'', 5.0E0, 5.0E0, 5.3273299999999999E0, 60.390999999999998E0),
    (2, N''This is a second default dummy game. This one takes place in fantasyland 2 (or probably in a real location once we have implemented the map).'', N''Registration'', N''Zombie game 002'', 10.0E0, 10.0E0, 10.32733E0, 65.391000000000005E0)');
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Description', N'GameState', N'Name', N'Nw_Ing', N'Nw_Lat', N'Se_Ing', N'Se_Lat') AND [object_id] = OBJECT_ID(N'[Games]'))
        SET IDENTITY_INSERT [Games] OFF;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'FirstName', N'IsAdmin', N'LastName') AND [object_id] = OBJECT_ID(N'[Users]'))
        SET IDENTITY_INSERT [Users] ON;
    EXEC(N'INSERT INTO [Users] ([Id], [FirstName], [IsAdmin], [LastName])
    VALUES (N''bobleif@example.com'', N''bob'', CAST(0 AS bit), N''leif''),
    (N''peternissan@example.com'', N''Peter'', CAST(0 AS bit), N''Nissan''),
    (N''karifjord@example.com'', N''Kari'', CAST(1 AS bit), N''Fjord'')');
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'FirstName', N'IsAdmin', N'LastName') AND [object_id] = OBJECT_ID(N'[Users]'))
        SET IDENTITY_INSERT [Users] OFF;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'DateTime', N'Description', N'GameId', N'Ing', N'IsHumanVisible', N'IsZombieVisible', N'Lat', N'Name', N'StartTime') AND [object_id] = OBJECT_ID(N'[Missions]'))
        SET IDENTITY_INSERT [Missions] ON;
    EXEC(N'INSERT INTO [Missions] ([Id], [DateTime], [Description], [GameId], [Ing], [IsHumanVisible], [IsZombieVisible], [Lat], [Name], [StartTime])
    VALUES (1, ''2015-12-25T00:00:00.0000000'', N''Mission desc'', 1, CAST(5.327336 AS real), CAST(1 AS bit), CAST(0 AS bit), CAST(60.391216 AS real), N''easy mission 1'', ''2014-12-25T00:00:00.0000000''),
    (2, ''2015-11-25T00:00:00.0000000'', N''Mission desc 2'', 1, CAST(5.327336 AS real), CAST(1 AS bit), CAST(0 AS bit), CAST(60.391216 AS real), N''easy mission 2'', ''2014-11-25T00:00:00.0000000'')');
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'DateTime', N'Description', N'GameId', N'Ing', N'IsHumanVisible', N'IsZombieVisible', N'Lat', N'Name', N'StartTime') AND [object_id] = OBJECT_ID(N'[Missions]'))
        SET IDENTITY_INSERT [Missions] OFF;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'BiteCode', N'GameId', N'IsHuman', N'IsPatientZero', N'UserId') AND [object_id] = OBJECT_ID(N'[Players]'))
        SET IDENTITY_INSERT [Players] ON;
    EXEC(N'INSERT INTO [Players] ([Id], [BiteCode], [GameId], [IsHuman], [IsPatientZero], [UserId])
    VALUES (1, N''AAA'', 1, CAST(1 AS bit), CAST(0 AS bit), N''bobleif@example.com''),
    (3, N''CCC'', 2, CAST(1 AS bit), CAST(0 AS bit), N''bobleif@example.com''),
    (2, N''BBB'', 1, CAST(1 AS bit), CAST(0 AS bit), N''peternissan@example.com''),
    (4, N''DDD'', 2, CAST(0 AS bit), CAST(1 AS bit), N''peternissan@example.com'')');
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'BiteCode', N'GameId', N'IsHuman', N'IsPatientZero', N'UserId') AND [object_id] = OBJECT_ID(N'[Players]'))
        SET IDENTITY_INSERT [Players] OFF;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'GameId', N'IsHuman', N'Name') AND [object_id] = OBJECT_ID(N'[Squads]'))
        SET IDENTITY_INSERT [Squads] ON;
    EXEC(N'INSERT INTO [Squads] ([Id], [GameId], [IsHuman], [Name])
    VALUES (1, 1, CAST(0 AS bit), N''The Squids''),
    (2, 2, CAST(1 AS bit), N''The Terrifiers'')');
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'GameId', N'IsHuman', N'Name') AND [object_id] = OBJECT_ID(N'[Squads]'))
        SET IDENTITY_INSERT [Squads] OFF;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'GameId', N'PlayerId', N'Rank', N'SquadId') AND [object_id] = OBJECT_ID(N'[SquadMembers]'))
        SET IDENTITY_INSERT [SquadMembers] ON;
    EXEC(N'INSERT INTO [SquadMembers] ([Id], [GameId], [PlayerId], [Rank], [SquadId])
    VALUES (1, 1, 1, 9, 1)');
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'GameId', N'PlayerId', N'Rank', N'SquadId') AND [object_id] = OBJECT_ID(N'[SquadMembers]'))
        SET IDENTITY_INSERT [SquadMembers] OFF;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'GameId', N'PlayerId', N'Rank', N'SquadId') AND [object_id] = OBJECT_ID(N'[SquadMembers]'))
        SET IDENTITY_INSERT [SquadMembers] ON;
    EXEC(N'INSERT INTO [SquadMembers] ([Id], [GameId], [PlayerId], [Rank], [SquadId])
    VALUES (2, 2, 2, 8, 2)');
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'GameId', N'PlayerId', N'Rank', N'SquadId') AND [object_id] = OBJECT_ID(N'[SquadMembers]'))
        SET IDENTITY_INSERT [SquadMembers] OFF;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    CREATE INDEX [IX_Kills_GameId] ON [Kills] ([GameId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    CREATE INDEX [IX_Kills_KillerId] ON [Kills] ([KillerId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    CREATE UNIQUE INDEX [IX_Kills_VictimId] ON [Kills] ([VictimId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    CREATE INDEX [IX_Missions_GameId] ON [Missions] ([GameId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    EXEC(N'CREATE UNIQUE INDEX [IX_Players_BiteCode] ON [Players] ([BiteCode]) WHERE [BiteCode] IS NOT NULL');
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    CREATE INDEX [IX_Players_GameId] ON [Players] ([GameId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    CREATE INDEX [IX_Players_UserId] ON [Players] ([UserId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    CREATE INDEX [IX_SquadMembers_GameId] ON [SquadMembers] ([GameId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    CREATE UNIQUE INDEX [IX_SquadMembers_PlayerId] ON [SquadMembers] ([PlayerId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    CREATE INDEX [IX_SquadMembers_SquadId] ON [SquadMembers] ([SquadId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    CREATE INDEX [IX_Squads_GameId] ON [Squads] ([GameId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220329104731_final')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20220329104731_final', N'5.0.15');
END;
GO

COMMIT;
GO


# Movie Characters API 

An Entity Framework Code First workflow and ASP.NET Core Web API in C#.

## Table of Contents
- [Installation](#installation)
- [Usage](#usage)
- [Description](#description)
- [Visuals](#visuals)
- [Maintainers](#maintainers)
- [Project status](#project-status)


## Installation

Install following tools:

* Visual Studio 2019 with .NET 5
* SQL Server Managment Studio

## Usage

First clone the git repository

    git clone git@gitlab.com:EsbenLB/humans-vs-zombies-backend.git

Open in Visual Studio.

Connect to local server (change this line inn appsettings.json"):

    "DefaultConnection": "Data Source = <<NAME OF CONNECTION STRING>>; Initial Catalog = MovieCharactersDb; Integrated Security = True;"

Open Package Manager Console in Visual Studio an run to create the database in LocalDb:

    update-database

Run program with button with the *green arrow and IIS Express* or *F5*

## Description

### Models
* DTOs - Contains Create, Read and Edit classes for Game, Kill, Mission, Player, Squad, SquadMember, User and SquandCheckin tables.  SquandCheckin are not used in the project at this state
* Data - Contains information about the database, like setting realtionship between tables and dummydata
* Domain - Contains the Game, Kill, Mission, Player, Squad, SquadMember, User and SquandCheckin tables in the database

### Migrations
* Conntains the information about hte database that will be created when running ```update-database```


### Profiles
Relation between the DTOs and the Domain

* GameProfile
* KillProfile
* MissionProfile
* PlayerProfile 
* MovieProfile 
* SquadCheckinProfile
* SquadMemberProfile
* SquadProfile
* UserProfile

### Controllers

Contains methods to get one, get all, create, update and delete from database.

* GameController
* KillController
* MissionController
* PlayerController
* SquadCheckinController
* SquadController
* UserController


### Services

Contains helping methods that frees the controllers

* GameService
* KillService
* MissionService
* PlayerService
* SquadCheckinService
* SquadService
* UserService


### Visuals
![Er Diagram](/Diagram/ERdiagram.png) 
ER Diagram for the Humen vs. Zombies database.

## Maintainers

Stian Selle - @stianstian95

Esben Bjarnason - @EsbenLB

Karianne Tesaker - @kariannet

Silja Stubhag Torkildsen - @SiljaTorki

## Project status

Almost complete! The minimal Viable Product for the assignmement works.

Features to add:
* Functionality to squad checkin
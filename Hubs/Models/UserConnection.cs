﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Hubs.Models
{
    public class UserConnection
    {
        public string User { get; set; }
        public string Room { get; set; }
    }
}

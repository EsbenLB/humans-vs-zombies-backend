﻿using Backend.Hubs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Backend.Hubs.Clients
{
    public interface IChatClient
    {
        Task ReceiveMessage(ChatMessage message);
        void addChatMessage(string v);

        //Task JoinRoom(UserConnection userConnection);
        //Task LeaveRoom(UserConnection userConnection);
    }
}

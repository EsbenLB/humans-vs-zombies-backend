using Backend.Models;
using Backend.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http; 
using Microsoft.AspNetCore.Authorization;
using Backend.Authorization;
using Backend.Hubs;
using System.Reflection;
using System.IO;
using Backend.Hubs.Models;

namespace Backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<HvsZDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddSignalR();
            services.AddControllers();
            services.AddCors(options =>
            {
                options.AddPolicy("mypolicy", policy =>
                {
                    policy.AllowAnyHeader()
                        .AllowAnyMethod()
                        .WithOrigins("http://localhost:3000")
                        .AllowCredentials();
                    policy.AllowAnyHeader()
                        .AllowAnyMethod()
                        .WithOrigins("https://noroff-zvsh-docker-heroku.herokuapp.com")
                        .AllowCredentials();
                    policy.AllowAnyHeader()
                        .AllowAnyMethod()
                        .WithOrigins("https://noroff-zvsh-docker-herokunew.herokuapp.com")
                        .AllowCredentials();
                });
            });
            
            // Authentication part start
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.Authority = Configuration["Auth0:Authority"];
                options.Audience = Configuration["Auth0:Audience"];
            });
            services.AddHttpClient();
            services.AddAuthorization(options =>
                options.AddPolicy("MustBeGameCreator", policy =>
                    policy.Requirements
                        .Add(new MustBeGameCreatorRequirement())));
            services.AddScoped<IAuthorizationHandler, MustBeGameCreatorHandler>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IDictionary<string, UserConnection>>(opts => new Dictionary<string, UserConnection>()); // signalR
            // Authentication part end
            services.AddAutoMapper(typeof(Startup));
            services.AddScoped(typeof(IGameService), typeof(GameService));
            services.AddScoped(typeof(IUserService), typeof(UserService));
            services.AddScoped(typeof(IPlayerService), typeof(PlayerService));
            services.AddScoped(typeof(IKillService), typeof(KillService));
            services.AddScoped(typeof(IMissionService), typeof(MissionService));
            services.AddScoped(typeof(ISquadService), typeof(SquadService));
            //services.AddScoped(typeof(ISquadCheckinService), typeof(SquadCheckinService));
            services.AddScoped(typeof(ISquadMemberService), typeof(SquadMemberService));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { 
                    Title = "Humans versus Zombies", 
                    Version = "v1",
                    Description = "Humans vs. Zombies is a game of tag. All players begin as humans, and one is randomly chosen to be the “Original Zombie.” The Original Zombie tags human players and turns them into zombies. Zombies must tag and eat a human every 48 hours or they starve to death and are out of the game.",
                    Contact = new OpenApiContact
                    {
                        Name = "Esben, Karianne, Silja & Stian",
                        Email = string.Empty,
                        Url = new Uri("https://gitlab.com/EsbenLB/humans-vs-zombies-backend")
                    }
                });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors("mypolicy");
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Backend v1");
                    }
                );
            }

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<ChatHub>("/hubs/chat");
            });
        }
    }
}

﻿using AutoMapper;
using Backend.Models;
using Backend.Models.Domain;
using Backend.Models.DTO.Player;
using Backend.Models.DTO.User;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [Authorize]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly HvsZDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        public UserController(HvsZDbContext context, IMapper mapper, IUserService userService)
        {
            _context = context;
            _mapper = mapper;
            _userService = userService;
        }
        /// <summary>
        /// Get all Users.
        /// </summary>
        /// <returns>All Users</returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetUsers()
        {
            var users = await _userService.GetAllUsersAsync();

            List<UserReadDTO> userRead = _mapper.Map<List<UserReadDTO>>(users);

            return userRead;
        }
        /// <summary>
        /// Get User with Id.
        /// </summary>
        /// <param name="id">User with Id</param>
        /// <returns>User or statuscode: 404</returns>
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<UserReadDTO>> GetUserById(string id)
        {
            var user = await _userService.GetUserAsync(id);

            if(user == null)
            {
                return NotFound("User does not exist!");
            }

            return _mapper.Map<UserReadDTO>(user);
        }
        /// <summary>
        /// Add User to database.
        /// </summary>
        /// <param name="user">User to add to database</param>
        /// <returns>StatusCode 201 or 500</returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult<UserReadDTO>> PostUser(UserCreateDTO user)
        {
            User domainUser = _mapper.Map<User>(user);

            try
            {
                await _userService.AddUserAsync(domainUser);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            UserReadDTO newUser = _mapper.Map<UserReadDTO>(domainUser);

            return CreatedAtAction("GetUserById", new { Id = newUser.Id }, newUser);
        }
        /// <summary>
        /// Delete User with Id from database.
        /// </summary>
        /// <param name="id">User with Id to delete</param>
        /// <returns>Status code: 204 or 404</returns>
        [AllowAnonymous]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteUser(string id)
        {
            var user = _userService.UserExists(id);

            if (user == false)
            {
                return NotFound("Failed to delete user, the user does not exist!");
            }

            await _userService.DeleteUserAsync(id);
            return NoContent();
        }
        /// <summary>
        /// Update User in database.
        /// </summary>
        /// <param name="id">Update User with Id</param>
        /// <param name="updatedUser">Updated version of User</param>
        /// <returns>Statuscode: 204 or 404</returns>
        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<ActionResult> PutUser(string id, UserUpdateDTO updatedUser)
        {
            if (_userService.UserExists(id) == false)
            {
                return BadRequest("The user you want to update does not exist.");
            }

            User domainUser = _mapper.Map<User>(updatedUser);
            domainUser.Id = id;
            await _userService.UpdateUserAsync(domainUser);

            return NoContent();
        }

        /// <summary>
        /// Get Players from User.
        /// </summary>
        /// <param name="id">Id of User to get Players from</param>
        /// <returns>Players or 500</returns>
        [AllowAnonymous]
        [HttpGet("{id}/players")]
        public async Task<ActionResult<List<PlayerReadDTO>>> GetPlayersByUser(string id)
        {
            var user = await _context.Users.Include(u => u.Players).FirstOrDefaultAsync(u => u.Id == id);

            if (user == null)
            {
                return NotFound("User does not exist!");
            }

            List<PlayerReadDTO> playerRead = _mapper.Map<List<PlayerReadDTO>>(user.Players.ToList());

            if (playerRead.Count() == 0)
            {
                return Content("The user does not have any players");
                
            }

            return playerRead;
        }
    }
}

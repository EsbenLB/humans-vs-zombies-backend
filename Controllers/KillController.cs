﻿using AutoMapper;
using Backend.Models;
using Backend.Models.Domain;
using Backend.Models.DTO.Kill;
using Backend.Models.DTO.Player;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [Authorize]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [Route("api/[controller]")]
    [ApiController]
    public class KillController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IKillService _killService;
        private readonly IPlayerService _playerService;

        public KillController(IMapper mapper, IKillService killService, IPlayerService playerService)
        {
            _mapper = mapper;
            _killService = killService;
            _playerService = playerService;
        }

        /// <summary>
        /// Get All kills.
        /// </summary>
        /// <returns>All Kills</returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<KillReadDTO>>> GetKills()
        {
            var kills = await _killService.GetAllKillsAsync();

            List<KillReadDTO> killRead = _mapper.Map<List<KillReadDTO>>(kills);

            return killRead;
        }

        /// <summary>
        /// Get Kill by Id.
        /// </summary>
        /// <param name="id">Id for the kill</param>
        /// <returns>Kill or 404</returns>
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<KillReadDTO>> GetKillById(int id)
        {
            var kill = await _killService.GetKillAsync(id);

            if (kill == null)
            {
                return NotFound("Kill does not exist!");
            }

            return _mapper.Map<KillReadDTO>(kill);
        }

        /// <summary>
        /// Add a Kill to database.
        /// </summary>
        /// <param name="kill">Kill to be added</param>
        /// <returns>Statuscode 201 or 500</returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult<KillReadDTO>> PostKill(KillCreateDTO kill)
        {
            Kill domainKill = _mapper.Map<Kill>(kill);

            try
            {
                await _killService.AddKillAsync(domainKill);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            KillReadDTO newKill = _mapper.Map<KillReadDTO>(domainKill);

            return CreatedAtAction("GetKillById", new { Id = newKill.Id }, newKill);
        }

        /// <summary>
        /// Delete Kill with id from database.
        /// </summary>
        /// <param name="id">Kill with Id to delete</param>
        /// <returns>Statuscode: 204 or 404</returns>
        [AllowAnonymous]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteKill(int id)
        {
            if (_killService.KillExists(id) == false)
            {
                return NotFound("Failed to delete kill, the kill does not exist!");
            }

            await _killService.DeleteKillAsync(id);
            return NoContent();
        }

        /// <summary>
        /// Update Kill in database.
        /// </summary>
        /// <param name="id">Update Kill with id</param>
        /// <param name="updatedKill">Updated version of Kill</param>
        /// <returns>Statuscode: 204 or 404</returns>
        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<ActionResult> PutKill(int id, KillUpdateDTO updatedKill)
        {
            if(_killService.KillExists(id) == false)
            {
                return BadRequest("The kill you want to update does not exist.");
            }

            Kill domainKill = _mapper.Map<Kill>(updatedKill);
            domainKill.Id = id;
            await _killService.UpdateKillAsync(domainKill);

            return NoContent();
        }

        /// <summary>
        /// Add Kill with Bitecode.
        /// </summary>
        /// <param name="gameId">Id of Game in database</param>
        /// <param name="createKill">Kill to be added</param>
        /// <returns>Statuscode: 201 or 500</returns>
        [AllowAnonymous]
        [HttpPost("{gameId}/kill")]
        public async Task<ActionResult<KillReadDTO>> PostKillWithBiteCode(int gameId, KillCreateDTO createKill)
        {
            var victim = await _playerService.GetPlayerByBiteCode(createKill.BiteCode);

            Kill domainKill = new()
            {
                GameId = gameId,
                KillerId = createKill.KillerId,
                VictimId = victim.Id
            };

            try
            {
                await _killService.AddKillAsync(domainKill);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            victim.IsHuman = false;

            try
            {
                await _playerService.UpdatePlayerAsync(victim);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);

            }
            KillReadDTO newKill = _mapper.Map<KillReadDTO>(domainKill);
            return CreatedAtAction("GetKillById", new { Id = newKill.Id }, newKill);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Backend.Models;
using Backend.Models.Domain;
using AutoMapper;
using Backend.Services;
using Backend.Models.DTO.Mission;
using System.Net.Mime;
using Microsoft.AspNetCore.Authorization;

namespace Backend.Controllers
{
    [Authorize]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [Route("api/[controller]")]
    [ApiController]
    public class MissionController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IMissionService _missionService;

        public MissionController(IMapper mapper, IMissionService missionService)
        {
            _mapper = mapper;
            _missionService = missionService;
        }

        /// <summary>
        /// Get all Missions.
        /// </summary>
        /// <returns>All Missions</returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MissionReadDTO>>> GetMissions()
        {
            var missions = await _missionService.GetAllMissionsAsync();

            List<MissionReadDTO> missionRead = _mapper.Map<List<MissionReadDTO>>(missions);

            return missionRead;
        }

        /// <summary>
        /// Get Mission by Id.
        /// </summary>
        /// <param name="id">Get Mission with Id</param>
        /// <returns>Mission or statuscode 404</returns>
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<MissionReadDTO>> GetMissionById(int id)
        {
            var mission = await _missionService.GetMissionAsync(id);

            if (mission == null)
            {
                return NotFound("Mission does not exist!");
            }

            return _mapper.Map<MissionReadDTO>(mission);
        }

        /// <summary>
        /// Update Mission in database.
        /// </summary>
        /// <param name="id">Update Mission with Id</param>
        /// <param name="updatedmission">Updated version of Mission</param>
        /// <returns>Statuscode: 204 or 404</returns>
        /// To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<ActionResult> PutMission(int id, MissionUpdateDTO updatedmission)
        {
            if (_missionService.MissionExists(id) == false)
            {
                return BadRequest("The mission you want to update does not exist.");
            }

            Mission domainMission = _mapper.Map<Mission>(updatedmission);
            domainMission.Id = id;
            await _missionService.UpdateMissionAsync(domainMission);

            return NoContent();
        }

        /// <summary>
        /// Add Mission to database.
        /// </summary>
        /// <param name="mission">Mission to add to database</param>
        /// <returns>StatusCode 201 or 500</returns>
        /// To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult<MissionReadDTO>> PostMission(MissionCreateDTO mission)
        {
            Mission domainMission = _mapper.Map<Mission>(mission);

            try
            {
                await _missionService.AddMissionAsync(domainMission);
            }
            catch 
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            MissionReadDTO newMission = _mapper.Map<MissionReadDTO>(domainMission);

            return CreatedAtAction("GetMissionById", new { Id = newMission.Id }, newMission);
        }

        /// <summary>
        /// Delete Mission with Id from database.
        /// </summary>
        /// <param name="id">Mission with Id to delete</param>
        /// <returns>StatusCode 201 or 404</returns>
        [AllowAnonymous]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMission(int id)
        {
            var mission = _missionService.MissionExists(id);

            if (mission == false)
            {
                return NotFound("Failed to delete mission, the mission does not exist!");
            }
            await _missionService.DeleteMissionAsync(id);
            return NoContent();
        }

        /// <summary>
        /// Get Players in a game.
        /// </summary>
        /// <param name="gameId">Id of game to get players</param>
        /// <returns>All players</returns>
        [AllowAnonymous]
        [HttpGet("{gameId}/getbyGameId")]
        public async Task<ActionResult<IEnumerable<MissionReadDTO>>> GetMissionInGame(int gameId)
        {
            var missions = await _missionService.GetMissionsByGameId(gameId);
            
            List<MissionReadDTO> missionRead = _mapper.Map<List<MissionReadDTO>>(missions);

            return missionRead;

        }

    }
}

﻿using AutoMapper;
using Backend.Models;
using Backend.Models.Domain;
using Backend.Models.DTO.Squad;
using Backend.Models.DTO.SquadMember;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [Authorize]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [Route("api/[controller]")]
    [ApiController]
    public class SquadController : ControllerBase
    {
        private readonly HvsZDbContext _context;
        private readonly IMapper _mapper;
        private readonly ISquadService _squadService;
        private readonly ISquadMemberService _squadMemberService; 

        public SquadController(HvsZDbContext context, IMapper mapper, ISquadService squadService, ISquadMemberService squadMemberService)
        {
            _context = context;
            _mapper = mapper;
            _squadService = squadService;
            _squadMemberService = squadMemberService;
        }

        /// <summary>
        /// Get All Squads.
        /// </summary>
        /// <returns>All Squads</returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SquadReadDTO>>> getSquads()
        {
            var squads = await _squadService.GetAllSquadsAsync();

            List<SquadReadDTO> squadRead = _mapper.Map<List<SquadReadDTO>>(squads);

            return squadRead;
        }

        /// <summary>
        /// Get Squad by Id.
        /// </summary>
        /// <param name="id">Get Squad with Id</param>
        /// <returns>Squad or 404</returns>
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<SquadReadDTO>> GetSquadById(int id)
        {
            var squad = await _squadService.GetSquadByIdAsync(id);

            if(squad == null)
            {
                return NotFound("Squad does not exist!");
            }

            return _mapper.Map<SquadReadDTO>(squad);
        }

        /// <summary>
        /// Get all squads a player is a member of.
        /// </summary>
        /// <param name="playerId">Get Squad with Id</param>
        /// <returns>Squad or 404</returns>
        [AllowAnonymous]
        [HttpGet("{playerId}/player")]
        public async Task<ActionResult<SquadReadDTO>> GetSquadByPlayerId(int playerId)
        {
            var squadMembers = await _squadMemberService.GetSquadMembersByPlayerIdAsync(playerId);

            if (squadMembers.Count() < 1)
            {
                return NotFound("This player is not a member of any squads!");
            }

            var squad = await _squadService.GetSquadByIdAsync(squadMembers.First().SquadId);

            if (squad == null)
            {
                return NotFound("Squad does not exist!");
            }

            return _mapper.Map<SquadReadDTO>(squad);
        }

        /// <summary>
        /// Add Squad to database. Registers player as a member of the squad.
        /// </summary>
        /// <param name="squad">Squad to add to the database</param>
        /// <param name="playerId">Squad to add to the database</param>
        /// <returns>Statuscode: 201 or 500</returns>
        [AllowAnonymous]
        [HttpPost("{playerId}")]
        public async Task<ActionResult<SquadReadDTO>> PostSquad(SquadCreateDTO squad, int playerId)
        {
            Squad domainSquad = _mapper.Map<Squad>(squad);

            try
            {
                await _squadService.AddSquadAsync(domainSquad);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            CreatedAtAction("GetSquadById", new { Id = domainSquad.Id }, domainSquad);

            try
            {
                SquadMember squadMember = new SquadMember
                {
                    GameId = domainSquad.GameId,
                    SquadId = domainSquad.Id,
                    Rank = 1,
                    PlayerId = playerId
                };
                await _squadMemberService.AddSquadMemberAsync(squadMember);
            } catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            Squad newSquad = await _squadService.GetSquadByIdAsync(domainSquad.Id);
            SquadReadDTO readSquad = _mapper.Map<SquadReadDTO>(newSquad);
            return readSquad;
        }

        /// <summary>
        /// Delte Squad with Id from database.
        /// </summary>
        /// <param name="id">Squad with Id to delete</param>
        /// <returns>Statuscode: 204 or 404</returns>
        [AllowAnonymous]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteSquad(int id)
        {
            var squad = _squadService.SquadExists(id);

            if (squad == false)
            {
                return NotFound("Failed to delete squad, the squad does not exist!");
            }

            await _squadService.DeleteSquadAsync(id);
            return NoContent();
        }

        /// <summary>
        /// Update Squad in database.
        /// </summary>
        /// <param name="id">Update Squad with Id</param>
        /// <param name="updatedSquad">Updated version of Squad</param>
        /// <returns>Statuscode: 204 or 404</returns>
        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<ActionResult> PutSquad(int id, SquadUpdateDTO updatedSquad)
        {
            if (_squadService.SquadExists(id) == false)
            {
                return BadRequest("The squad you want to update does not exist.");
            }

            Squad domainSquad = _mapper.Map<Squad>(updatedSquad);
            domainSquad.Id = id;
            await _squadService.UpdateSquadAsync(domainSquad);

            return NoContent();
        }


        /// <summary>
        /// Get all SquadMembers in a squad
        /// </summary>
        /// <param name="squadId">Id of Game</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("{squadId}/getAllMembersbySquadId")]
        public async Task<ActionResult<IEnumerable<SquadMemberReadDTO>>> GetSquadMemberInSquad(int squadId)
        {
            var squadmembers = await _squadService.GetSquadmemberBySquadId(squadId);

            List<SquadMemberReadDTO> squadmemberRead = _mapper.Map<List<SquadMemberReadDTO>>(squadmembers);

            return squadmemberRead;

        }

        /// <summary>
        /// Get all Squads in a game
        /// </summary>
        /// <param name="gameId">Id of Game</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("{gameId}/getbyGameId")]
        public async Task<ActionResult<IEnumerable<SquadReadDTO>>> GetSquadInGame(int gameId)
        {
            var squads = await _squadService.GetSquadsByGameId(gameId);

            List<SquadReadDTO> squadRead = _mapper.Map<List<SquadReadDTO>>(squads);

            return squadRead;

        }

        /// <summary>
        ///     Get all Killed squad members
        /// </summary>
        /// <param name="squadId"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("{squadId}/getallKilledSquaddMembers")]
        public async Task<ActionResult<IEnumerable<int>>> GetKilledSquadMembersId(int squadId)
        {
            var killedMembers = await _squadService.GetKilledSquadMembersId(squadId);

            List<int> squadMemberRead = _mapper.Map<List<int>>(killedMembers);

            return squadMemberRead;

        }
    }
}

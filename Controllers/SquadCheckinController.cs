﻿//using AutoMapper;
//using Backend.Models;
//using Backend.Models.Domain;
//using Backend.Models.DTO.SquadCheckin;
//using Backend.Services;
//using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net.Mime;
//using System.Threading.Tasks;

//namespace Backend.Controllers
//{
//    [Authorize]
//    [ApiConventionType(typeof(DefaultApiConventions))]
//    [Produces(MediaTypeNames.Application.Json)]
//    [Consumes(MediaTypeNames.Application.Json)]
//    [Route("api/[controller]")]
//    [ApiController]
//    public class SquadCheckinController : ControllerBase
//    {
//        private readonly HvsZDbContext _context;
//        private readonly IMapper _mapper;
//        private readonly ISquadCheckinService _squadCheckinService;

//        public SquadCheckinController(HvsZDbContext context, IMapper mapper, ISquadCheckinService squadCheckin)
//        {
//            _context = context;
//            _mapper = mapper;
//            _squadCheckinService = squadCheckin;
//        }

//        /// <summary>
//        /// Get all SquadCheckins.
//        /// </summary>
//        /// <returns>All SquadCheckins</returns>
//        [AllowAnonymous]
//        [HttpGet]
//        public async Task<ActionResult<IEnumerable<SquadCheckinReadDTO>>> getSquadCheckins()
//        {
//            var squadCheckins = await _squadCheckinService.GetAllSquadCheckinAsync();
//            List<SquadCheckinReadDTO> squadCheckinRead = _mapper.Map<List<SquadCheckinReadDTO>>(squadCheckins);
//            return squadCheckinRead;
//        }

//        /// <summary>
//        /// Get SquadCheckins by Id.
//        /// </summary>
//        /// <param name="id">Get SquadCheckin with Id</param>
//        /// <returns>SquadCheckin or Statuscode 404</returns>
//        [AllowAnonymous]
//        [HttpGet("{id}")]
//        public async Task<ActionResult<SquadCheckinReadDTO>> GetSquadCheckinById(int id)
//        {
//            var squadCheckin = await _squadCheckinService.GetSquadCheckinAsync(id);
//            if(squadCheckin == null)
//            {
//                return NotFound("Squad checkin does not exist!");
//            }
//            return _mapper.Map<SquadCheckinReadDTO>(squadCheckin);
//        }

//        /// <summary>
//        /// Add SquadCheckin to database.
//        /// </summary>
//        /// <param name="squadCheckin">SquadCheckin to add to database</param>
//        /// <returns>Statuscode: 201 or 500</returns>
//        [AllowAnonymous]
//        [HttpPost]
//        public async Task<ActionResult<SquadCheckinReadDTO>> PostSquadCheckin(SquadCheckinCreateDTO squadCheckin)
//        {
//            SquadCheckin domainSquadCheckin = _mapper.Map<SquadCheckin>(squadCheckin);

//            try
//            {
//                await _squadCheckinService.AddSquadCheckinAsync(domainSquadCheckin);
//            }
//            catch
//            {
//                return StatusCode(StatusCodes.Status500InternalServerError);
//            }

//            SquadCheckinReadDTO newSquadCheckin = _mapper.Map<SquadCheckinReadDTO>(domainSquadCheckin);

//            return CreatedAtAction("GetSquadCheckinById", new { Id = newSquadCheckin.Id }, newSquadCheckin);
//        }

//        /// <summary>
//        /// Delete SquadCheckin with Id from database.
//        /// </summary>
//        /// <param name="id">SquadCheckin with Id to delete</param>
//        /// <returns>Statuscode: 204 or 404</returns>
//        [AllowAnonymous]
//        [HttpDelete("{id}")]
//        public async Task<ActionResult> DeleteSquadCheckin(int id)
//        {
//            var squadCheckin = _squadCheckinService.SquadCheckinExists(id);

//            if (squadCheckin == false)
//            {
//                return NotFound("Failed to delete squad checkin, the squad does not exist!");
//            }

//            await _squadCheckinService.DeleteSquadCheckin(id);
//            return NoContent();
//        }

//        /// <summary>
//        /// Update SquadCheckin in database.
//        /// </summary>
//        /// <param name="id">Update SquadCheckin with Id</param>
//        /// <param name="updatedSquadCheckin">Updated verion of SquadCheckin</param>
//        /// <returns>Statuscode: 204 or 404</returns>
//        [AllowAnonymous]
//        [HttpPut("{id}")]
//        public async Task<ActionResult> PutSquadCheckin(int id, SquadCheckinUpdateDTO updatedSquadCheckin)
//        {
//            if (_squadCheckinService.SquadCheckinExists(id) == false)
//            {
//                return BadRequest("The squad checkin you want to update does not exist.");
//            }

//            SquadCheckin domainSquadCheckin = _mapper.Map<SquadCheckin>(updatedSquadCheckin);
//            domainSquadCheckin.Id = id;
//            await _squadCheckinService.UpdateSquadCheckinAsync(domainSquadCheckin);

//            return NoContent();
//        }
//    }
//}

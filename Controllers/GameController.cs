﻿
using AutoMapper;
using Backend.Models;
using Backend.Models.Domain;
using Backend.Models.DTO.Game;
using Backend.Models.DTO.Kill;
using Backend.Models.DTO.Player;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Mime;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web.Http.Cors;

namespace Backend.Controllers
{
    /// <summary>
    /// API end points for character.
    /// </summary>
    [Authorize]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiController]
    [Route("api/[controller]")]
    public class GameController : ControllerBase
    {
        private readonly HvsZDbContext _context;
        private readonly IMapper _mapper;
        private readonly IGameService _gameService;
        private readonly IHttpClientFactory _clientFactory; // auth stuff
        private readonly string _auth0UserInfo; // auth stuff
        
        public GameController(HvsZDbContext context, 
            IMapper mapper, 
            IGameService gameService, 
            IHttpClientFactory clientFactory,
            IConfiguration configuration)
        {
            _context = context;
            _mapper = mapper;
            _gameService = gameService;
            _clientFactory = clientFactory;
            _auth0UserInfo = $"{configuration["Auth0:Authority"]}userinfo";
        }

        /// <summary>
        /// Get all Games.
        /// </summary>
        /// <returns>All Games</returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GameReadDTO>>> GetGames()
        {
            var games = await _gameService.GetAllGamesAsync();

            List<GameReadDTO> gameRead = _mapper.Map<List<GameReadDTO>>(games);

            return gameRead;
        }
        /// <summary>
        /// Get Game by id.
        /// </summary>
        /// <param name="id">Get Game with id</param>
        /// <returns>Game or statuscode 404</returns>
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<GameReadDTO>> GetGameById(int id)
        {
            var game = await _gameService.GetGameByIdAsync(id);

            if (game == null)
            {
                return NotFound("Game does not exist!");
            }

            return _mapper.Map<GameReadDTO>(game);
        }
        /// <summary>
        /// Add Game to database.
        /// </summary>
        /// <param name="game">Game to add to database</param>
        /// <returns>StatusCode 201 or 500</returns>
        ///[Authorize(Policy = "MustBeGameCreator")]
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult<GameReadDTO>> PostGame(GameCreateDTO game)
        {
            Game domainGame = _mapper.Map<Game>(game);

            try
            {
                await _gameService.AddGameAsync(domainGame);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            GameReadDTO newGame = _mapper.Map<GameReadDTO>(domainGame);

            return CreatedAtAction("GetGameById", new { Id = newGame.Id }, newGame);
        }
        /// <summary>
        /// Delete Game with id from database.
        /// </summary>
        /// <param name="id">Game with id to delete</param>
        /// <returns>Status code: 204 or 404</returns>
        ///[Authorize(Policy = "MustBeGameCreator")]
        [AllowAnonymous]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteGame(int id)
        {
            //var game = _gameService.GameExists(id);

            if (_gameService.GameExists(id) == false)
            {
                return NotFound("Failed to delete game, the game does not exist!");
            }

            await _gameService.DeleteGameAsync(id);
            return NoContent();
        }
        /// <summary>
        /// Update Game in database.
        /// </summary>
        /// <param name="id">Update game with id</param>
        /// <param name="updatedGame">Updated version of Game</param>
        /// <returns>Statuscode: 204 or 404</returns>
        ///[Authorize(Policy = "MustBeGameCreator")]
        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<ActionResult> PutGame(int id, GameCreateDTO updatedGame)
        {
            if (_gameService.GameExists(id) == false)
            {
                return BadRequest("The game you want to update does not exist.");
            }

            Game domainGame = _mapper.Map<Game>(updatedGame);
            domainGame.Id = id;
            await _gameService.UpdateGameAsync(domainGame);

            return NoContent();
        }

        /// <summary>
        /// Update Game state in database.
        /// </summary>
        /// <param name="id">Update game with id</param>
        /// <param name="updatedGame">Updated version of Game</param>
        /// <returns>Statuscode: 204 or 404</returns>
        [AllowAnonymous]
        [HttpPut("GameState/{id}")]
        public async Task<ActionResult> UpdateGameState(int id, GameUpdateStateDTO updatedGame)
        {
            if (_gameService.GameExists(id) == false)
            {
                return BadRequest("Failed to update the game state!");
            }

            Game domainGame = _mapper.Map<Game>(updatedGame);
            domainGame.Id = id;
            await _gameService.UpdateGameAsync(domainGame);

            return NoContent();
        }

        /// <summary>
        /// Gets players in a game.
        /// </summary>
        /// <param name="id">Id for the game</param>
        /// <returns>Players or message to inform that there is no players in the game</returns>
        [AllowAnonymous]
        [HttpGet("{id}/players")]
        public async Task<ActionResult<List<PlayerReadDTO>>> GetPlayersByGame(int id)
        {
            var game = await _context.Games.Include(c => c.Players).FirstOrDefaultAsync(c => c.Id == id);
            
            if (game == null)
            {
                return Content("There is no players in this game!");
            }

            return _mapper.Map<List<PlayerReadDTO>>(game.Players.ToList());
        }

    }
}

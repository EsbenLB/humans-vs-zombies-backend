﻿using AutoMapper;
using Backend.Models;
using Backend.Models.Domain;
using Backend.Models.DTO.Player;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [Authorize]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [Route("api/[controller]")]
    [ApiController]
    public class PlayerController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IPlayerService _playerService;

        public PlayerController(IMapper mapper, IPlayerService playerService)
        {
            _mapper = mapper;
            _playerService = playerService;
        }

        /// <summary>
        /// Get all Players.
        /// </summary>
        /// <returns>All Players</returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PlayerReadDTO>>> GetPlayers()
        {
            var players = await _playerService.GetAllPlayersAsync();

            List<PlayerReadDTO> playerRead = _mapper.Map<List<PlayerReadDTO>>(players);

            return playerRead;
        }

        /// <summary>
        /// Get Player by Id.
        /// </summary>
        /// <param name="id">Get Player with Id</param>
        /// <returns>Player or statuscode 404</returns>
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<PlayerReadDTO>> GetPlayerById(int id)
        {
            var player = await _playerService.GetPlayerAsync(id);

            if (player == null)
            {
                return NotFound("Player does not exist!");
            }

            return _mapper.Map<PlayerReadDTO>(player);
        }

        /// <summary>
        /// Add Player to database.
        /// </summary>
        /// <param name="player">Player to add to database</param>
        /// <returns>Statuscode 201 or 500</returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult<PlayerReadDTO>> PostPlayer(PlayerCreateDTO player)
        {
            Player domainPlayer = _mapper.Map<Player>(player);
            domainPlayer.BiteCode = GetBiteCode(8);

            try
            {
                await _playerService.AddPlayerAsync(domainPlayer);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            PlayerReadDTO newPlayer = _mapper.Map<PlayerReadDTO>(domainPlayer);

            return CreatedAtAction("GetPlayerById", new { Id = newPlayer.Id }, newPlayer);
        }

        /// <summary>
        /// Make a random string of given length. Helper function.
        /// </summary>
        /// <param name="length">length of the string to be created</param>
        /// <returns>BiteCode</returns>
        private string GetBiteCode(int length)
        {
            string biteCode = "";
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            Random rand = new Random();
            int randNumber;

            for (int i = 0; i < length; i++)
            {
                randNumber = rand.Next(0, 62);
                if (randNumber >= chars.Length)
                {
                    // add lowercase
                    biteCode += chars.ToLower()[randNumber - chars.Length];
                }
                else
                {
                    biteCode += chars[randNumber];
                }
            }

            return biteCode;
        }

        /// <summary>
        /// Delete Player with Id from database.
        /// </summary>
        /// <param name="id">Player with Id to delete</param>
        /// <returns>Statuscode: 204 or 404</returns>
        [AllowAnonymous]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeletePlayer(int id)
        {
            if (_playerService.PlayerExists(id) == false)
            {
                return NotFound("Failed to delete player, the player does not exist!");
            }

            await _playerService.DeletePlayerAsync(id);
            return NoContent();
        }

        /// <summary>
        /// Update Player in database.
        /// </summary>
        /// <param name="id">Update Player with Id</param>
        /// <param name="updatedPlayer">Updated version of Player</param>
        /// <returns>Statuscode: 204 or 404</returns>
        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<ActionResult> PutPlayer(int id, PlayerUpdateDTO updatedPlayer)
        {

            Player oldPlayer = await _playerService.GetPlayerAsync(id);

            if (oldPlayer == null)
            {
                return NotFound("The player you want to update does not exist.");
            }

            oldPlayer.IsHuman = updatedPlayer.IsHuman;
            oldPlayer.IsPatientZero = updatedPlayer.IsPatientZero;
    
            await _playerService.UpdatePlayerAsync(oldPlayer);

            return NoContent();
        }

        /// <summary>
        /// Update Player in a given Game.
        /// </summary>
        /// <param name="playerId">Id of Player</param>
        /// <param name="game">Id of Game</param>
        /// <returns>Statuscode: 204 or 404</returns>
        [AllowAnonymous]
        [HttpPut("{id}/game")]
        public async Task<IActionResult> UpdatePlayerGame(int playerId, int game)
        {
            if (_playerService.PlayerExists(playerId) == false)
            {
                return NotFound("The player you want to update does not exist.");
            }

            try
            {
                await _playerService.UpdatePlayerGameAsync(playerId, game);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid Game.");
            }

            return NoContent();
        }

        /// <summary>
        /// Update Player by a given User.
        /// </summary>
        /// <param name="playerId">Id of Player to be updated</param>
        /// <param name="user">Id of User</param>
        /// <returns>Statuscode: 204 or 404</returns>
        [AllowAnonymous]
        [HttpPut("{id}/user")]
        public async Task<IActionResult> UpdatePlayerUser(int playerId, int user)
        {
            if (_playerService.PlayerExists(playerId) == false)
            {
                return NotFound("The player you want to update does not exist.");
            }

            try
            {
                await _playerService.UpdatePlayerGameAsync(playerId, user);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid User.");
            }

            return NoContent();
        }

        /// <summary>
        /// Get Player by a given BiteCode.
        /// </summary>
        /// <param name="biteCode">The string of BiteCode</param>
        /// <returns>Player or Statuscode: 404</returns>
        [AllowAnonymous]
        [HttpGet("{biteCode}/getByBiteCode/")]
        public async Task<ActionResult<PlayerReadDTO>> GetPlayerByBiteCode(string biteCode)
        {
            var player = await _playerService.GetPlayerByBiteCode(biteCode);

            if (player == null)
            {
                return NotFound("There exists no players with the given BiteCode");
            }

            PlayerReadDTO playerRead = _mapper.Map<PlayerReadDTO>(player);

            return playerRead;
        }

        /// <summary>
        /// Get all Players in a game
        /// </summary>
        /// <param name="gameId">Id of Game</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("{gameId}/getbyGameId")]
        public async Task<ActionResult<IEnumerable<PlayerReadDTO>>> GetPlayerInGame(int gameId)
        {
            var players = await _playerService.GetPlayerByGameId(gameId);
         
            if(players.Count() == 0)
            {
                return Content("There is no players in this game!");
            }

            List<PlayerReadDTO> playerRead = _mapper.Map<List<PlayerReadDTO>>(players);

            return playerRead;

        }

        
    }
}

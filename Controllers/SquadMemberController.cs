﻿using AutoMapper;
using Backend.Models;
using Backend.Models.Domain;
using Backend.Models.DTO.SquadMember;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [Authorize]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [Route("api/[controller]")]
    [ApiController]
    public class SquadMemberController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ISquadMemberService _squadMemberService;

        public SquadMemberController(IMapper mapper, ISquadMemberService squadMemberService)
        {
            _mapper = mapper;
            _squadMemberService = squadMemberService;
        }

        /// <summary>
        /// Get all SquadMembers.
        /// </summary>
        /// <returns>All SquadMembers</returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SquadMemberReadDTO>>> getSquadMembers()
        {
            var squadMembers = await _squadMemberService.GetAllSquadMembersAsync();
            List<SquadMemberReadDTO> squadMemberRead = _mapper.Map<List<SquadMemberReadDTO>>(squadMembers);

            return squadMemberRead;
        }

        /// <summary>
        /// Get SquadMemeber by Id.
        /// </summary>
        /// <param name="id">SquadMember with Id</param>
        /// <returns>SquadMember or statuscode 404</returns>
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<SquadMemberReadDTO>> GetSquadMemberById(int id)
        {
            var squadMember = await _squadMemberService.GetSquadMemberAsync(id);
            if(squadMember == null)
            {
                return NotFound("Squad member does not exist!");
            }
            return _mapper.Map<SquadMemberReadDTO>(squadMember);
        }

        /// <summary>
        /// Add SquadMemeber to database.
        /// </summary>
        /// <param name="squadMember">SquadMember to add to database</param>
        /// <returns>Statuscode: 201 or 500</returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult<SquadMemberReadDTO>> PostSquadMember(SquadMemberCreateDTO squadMember)
        {
            SquadMember domainSquadMember = _mapper.Map<SquadMember>(squadMember);

            try
            {
                await _squadMemberService.AddSquadMemberAsync(domainSquadMember);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            SquadMemberReadDTO newSquadMember = _mapper.Map<SquadMemberReadDTO>(domainSquadMember);
            return CreatedAtAction("GetSquadMemberById", new { Id = newSquadMember.Id }, newSquadMember);
        }

        /// <summary>
        /// Delete SquadMember with Id from database.
        /// </summary>
        /// <param name="id">SquadMemeber with Id to delete</param>
        /// <returns>Statuscode: 204 or 404</returns>
        [AllowAnonymous]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteSquadMember(int id)
        {
            var squadMember = _squadMemberService.SquadMemberExists(id);
            if (squadMember == false)
            {
                return NotFound("Failed to delete squad member, the squad member does not exist!");
            }
            await _squadMemberService.DeleteSquadMember(id);
            return NoContent();
        }

        /// <summary>
        /// Update SquadMember in database.
        /// </summary>
        /// <param name="id">Update SquadMember with Id</param>
        /// <param name="updatedSquadMember">Updated version of SquadMember</param>
        /// <returns>Statuscode: 204 or 404</returns>
        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<ActionResult> PutSquadMember(int id, SquadMemberUpdateDTO updatedSquadMember)
        {
            if(_squadMemberService.SquadMemberExists(id) == false)
            {
                return BadRequest("The squad member you want to update does not exist.");
            }

            SquadMember domainSquadMember = _mapper.Map<SquadMember>(updatedSquadMember);
            domainSquadMember.Id = id;
            await _squadMemberService.UpdateSquadMember(domainSquadMember);

            return NoContent();
        }
    }
}
